import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(5)

WebUI.verifyElementPresent(findTestObject('BlanjaBeli/select_Category'), 0)

WebUI.selectOptionByLabel(findTestObject('BlanjaBeli/select_Category'), 'Komputer & Laptop', false)

WebUI.setText(findTestObject('BlanjaBeli/input_search'), 'macbook')

WebUI.click(findTestObject('BlanjaBeli/btn_Search'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('BlanjaBeli/select_sort_item'))

WebUI.click(findTestObject('BlanjaBeli/selected_rating'))

WebUI.scrollToPosition(150, 300)

WebUI.delay(10)

WebUI.click(findTestObject('BlanjaBeli/item_selected'))

WebUI.delay(2)

WebUI.click(findTestObject('BlanjaBeli/add_to_Cart'))

WebUI.setText(findTestObject('BlanjaBeli/input_search'), 'senka')

WebUI.selectOptionByLabel(findTestObject('BlanjaBeli/select_Category'), 'Kesehatan & Kecantikan', false)

WebUI.click(findTestObject('BlanjaBeli/btn_Search'))

WebUI.delay(2)

WebUI.click(findTestObject('BlanjaBeli/item_selected_senka'))

WebUI.delay(2)

WebUI.click(findTestObject('BlanjaBeli/add_to_Cart'))

WebUI.mouseOver(findTestObject('BlanjaBeli/cart'))

WebUI.click(findTestObject('BlanjaBeli/to_cart'))

