import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('www.blanja.com')

WebUI.maximizeWindow()

WebUI.verifyElementPresent(findTestObject('BlanjaBeli/select_Category'), 0)

WebUI.click(findTestObject('BlanjaLogin/btn_close_guide'))

WebUI.verifyElementPresent(findTestObject('BlanjaLogin/btn_Masuk'), 0)

WebUI.delay(3)

WebUI.click(findTestObject('BlanjaLogin/btn_Masuk'))

WebUI.delay(2)

WebUI.click(findTestObject('BlanjaLogin/btn_login_facebook'))

WebUI.switchToWindowIndex(1)

WebUI.delay(3)

WebUI.setText(findTestObject('BlanjaLogin/input_email'), 'abifdhllh@gmail.com')

WebUI.setEncryptedText(findTestObject('BlanjaLogin/input_password'), 'lbAT+ZXL0Z5Mp1vyZZ4lOw==')

WebUI.click(findTestObject('BlanjaLogin/btn_loginto_facebook'), FailureHandling.STOP_ON_FAILURE)

WebUI.switchToWindowIndex(0)

