<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>item_selected_senka</name>
   <tag></tag>
   <elementGuidId>5e9c923c-74a2-4170-9e70-2872b58df383</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#queryItemPageForm > div.list-container-content.result-thumbs.clearfix.result-box > div.category-recommended.clearfix > div:nth-child(2) > div.product-desc > a > h3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
