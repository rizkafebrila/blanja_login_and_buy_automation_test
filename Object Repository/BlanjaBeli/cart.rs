<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>cart</name>
   <tag></tag>
   <elementGuidId>9c6d7c1c-de6d-4866-a91a-000e4e7b3927</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@class = 'headerv2-support__menu-link  my-cart user-cart clearfix']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#headerv2 > div > div.headerv2-bottom > div > div.headerv2-right.clearfix > div > div.headerv2-support__menu.headerv2-support__cart > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>headerv2-support__menu-link  my-cart user-cart clearfix</value>
   </webElementProperties>
</WebElementEntity>
