<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>add_to_Cart</name>
   <tag></tag>
   <elementGuidId>5752a113-f55b-4cc2-8899-645a1c0eb596</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>body > div.clearfix.pdpv2-canvas > div.pdpv2 > div.container > div.row.section.properties > div.col-9.left > div.row.photo-props > div.col-8 > div.row.ctas > div > div > div > div:nth-child(2) > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>pdpv2-button pdpv2-button--white pdpv2-w100 call-to-action-button</value>
   </webElementProperties>
</WebElementEntity>
