<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>item_selected</name>
   <tag></tag>
   <elementGuidId>c5fe8979-e589-44e1-86dd-412b5ecbdbe5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//h3[@class = 'product-name' and @label = 'Baseus Sky Case Apple The New Macbook Air 12 Inch Hardcase']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#queryItemPageForm > div.list-container-content.result-thumbs.clearfix.result-box > div.category-recommended.clearfix > div:nth-child(4) > div.product-desc > a > h3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>product-name</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>label</name>
      <type>Main</type>
      <value>Baseus Sky Case Apple The New Macbook Air 12 Inch Hardcase</value>
   </webElementProperties>
</WebElementEntity>
